
window.onload = () => {

    startGame(8, 8, 10);
    //функція запуску гри
    function startGame(width, height, bombs_count) {
        //створюємо поле
        const field = document.querySelector('.field');
        const cellsCount = width * height;
        field.innerHTML = '<button></button>'.repeat(cellsCount);
        const cells = [...field.children];
        let closedCount = cellsCount;
        //лічильник прапорів
        let fc = 0;
        //створюємо міни 
        const bombs = [...Array(cellsCount).keys()]
            .sort(() => Math.random() - 0.5)
            .slice(0, bombs_count);
        //при кліку лівоє кнопкою
        field.addEventListener('click', (event) => {
            //якщо не кнопка, не реагуємо
            if (event.target.tagName !== 'BUTTON') {
                return;
            }
            //функція нова гра
            newGame();
            //вираховуємо обрану кнопку
            const index = cells.indexOf(event.target);
            const column = index % width;
            const row = Math.floor(index / width);
            open(row, column);
        });
        //створюємо прапори по кліку правою кнопкою
        field.addEventListener("contextmenu", (e) => {
            e.preventDefault();
            const el = e.target;
            let ind = cells.indexOf(el);

            newGame();
            // якщо не кнопка, не реагуємо
            if (e.target.tagName !== 'BUTTON') {
                return;
            }
            //якщо не нажимали, не реагуємо
            if (el.disabled === true) { return; };
            //якщо був прапор, прибираємо
            if (el.classList.contains("flag")) {
                el.classList.remove("flag");
                fc--;
            }
            // в інших випадках ставимо прапор
            else {
                el.classList.add("flag");
                fc++;
            }
            // створюємо лічильник прапорів і мін        
            const schet = document.querySelector(".flag_count");
            schet.innerHTML = `Прапори: ${fc} / Міни: ${bombs_count}`;
            e.stopPropagation();
        });
        //перевіряємо
        function isValid(row, column) {
            return row >= 0
                && row < height
                && column >= 0
                && column < width;
        }
        //фуункція захвату кнопок
        function getCount(row, column) {
            let count = 0;
            for (let x = -1; x <= 1; x++) {
                for (let y = -1; y <= 1; y++) {
                    if (isBomb(row + y, column + x)) {
                        count++;
                    }
                }
            }
            return count;
        }
        //функція реаукії на клік лівою кнопкою
        function open(row, column) {

            if (!isValid(row, column)) return;
            //індекс
            const index = row * width + column;
            const cell = cells[index];
            //якщо нажимали на прапор, не реагуємо
            if (cell.disabled === true) return;
            if (cell.classList.contains("flag") === true) return;
            //кнопка нажата та вже не рееагує
            cell.disabled = true;
            //якщо міна
            if (isBomb(row, column)) {
                cell.classList.add("bomb");
                cells.forEach(e => {
                    //шукаємо всі міни і деактивуємо всі кнопки
                    let ind = cells.indexOf(e);
                    cells[ind].disabled = true;
                    if (bombs.includes(ind)) {
                        cells[ind].classList.add("bomb");
                    }
                })
                alert('Нажаль, ви програли!');
                return;
            };
            //зменшуємо кількість нажатих кнопок
            closedCount--;
            //якщо кількість відкритих осередків менше або дорівнює кількості мін, ви перемогли
            if (closedCount <= bombs_count) {
                alert('Супер, ви перемогли!');
            }
            //відкриваємо поруч кнопки якщо кнопка = 0
            const count = getCount(row, column);
            if (count !== 0) {
                cell.innerHTML = count;
                return;
            }
            //відкриваємо поруч кнопки
            for (let x = -1; x <= 1; x++) {
                for (let y = -1; y <= 1; y++) {
                    open(row + y, column + x);
                }
            }
        };
        //функція індексу мін
        function isBomb(row, column) {
            if (!isValid(row, column)) return false;
            const index = row * width + column;

            return bombs.includes(index);
        };
    };

    function newGame() {
        const newG = document.querySelector(".newGame");;
        newG.innerHTML = `Нова гра`;
        newG.addEventListener('click', (event) => {
            location.reload();
        });
    }
}

