
let divDisplay = document.getElementById(("display")),
    btnstop = document.getElementById("btnstop"),
    start = document.getElementById("btnstart"),
    reset = document.getElementById("btnreset"),
    sec = 0,
    min = 0,
    hrs = 0,
    intervalTaiming,
    flagStart = 0;

const stopwatch = () => {
    sec++;
    if (sec >= 60) {
        sec = 0;
        min++;
        if (min >= 60) {
            min = 0;
            hrs++;
        }
    }
    document.getElementById("hour").textContent = hrs;
    document.getElementById("minutes").textContent = min;
    document.getElementById("seconds").textContent = sec;
};



const pressStart = () => {
    if (flagStart == 0) {
        divDisplay.classList.remove("dispaly-color");
        divDisplay.classList.remove("red");
        divDisplay.classList.remove("silver");
        divDisplay.classList.add("green");
        intervalTaiming = setInterval(stopwatch, 1000);
        flagStart = 1;
    }
};
const pressStop = () => {
    divDisplay.classList.remove("dispaly-color");
    divDisplay.classList.remove("green");
    divDisplay.classList.remove("silver");
    divDisplay.classList.add("red");
    clearInterval(intervalTaiming);
    flagStart = 0;

};

const pressReset = () => {
    divDisplay.classList.remove("dispaly-color");
    divDisplay.classList.remove("green");
    divDisplay.classList.remove("red");
    divDisplay.classList.add("silver");
    document.getElementById("hour").textContent = "00";
    document.getElementById("minutes").textContent = "00";
    document.getElementById("seconds").textContent = "00";
    clearInterval(intervalTaiming);
    sec = 0;
    min = 0;
    hrs = 0;
};

btnstop.onclick = pressStop;
start.onclick = pressStart;
reset.onclick = pressReset;

