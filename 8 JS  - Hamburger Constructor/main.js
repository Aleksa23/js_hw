const [
  SIZE_SMALL,
  SIZE_LARGE,
  STUFFING_CHEESE,
  STUFFING_SALAD,
  STUFFING_POTATO,
  TOPPING_MAYO,
  TOPPING_SPICE,
] = [
  { id: "small", hr: 50, kal: 20 },
  { id: "large", hr: 100, kal: 40 },
  { id: "cheese", hr: 10, kal: 20 },
  { id: "salad", hr: 20, kal: 5 },
  { id: "potato", hr: 15, kal: 10 },
  { id: "mayo", hr: 20, kal: 5 },
  { id: "spice", hr: 15, kal: 0 },
];

class Hamburger {
  constructor(size, shuffing) {
    this.size = size;
    this.shuffing = shuffing;
    this.topping = [];
  }
  get size() {
    return this._size;
  }
  set size(size) {
    if (size === SIZE_LARGE || size === SIZE_SMALL) {
      this._size = size;
    } else {
      console.log(
        new Error(
          "Об'єкт не створено. Введено неіснуючі дані (введіть SIZE_LARGE або SIZE_SMALL)"
        )
      );
    }
    try {
      if (!size) {
        throw new Error(
          "Не ведено або введено неіснуючі дані (введіть SIZE_LARGE або SIZE_SMALL)"
        );
      }
    } catch (error) {
      console.log(error);
    }
  }
  get shuffing() {
    return this._size;
  }
  set shuffing(shuffing) {
    if (
      shuffing === STUFFING_CHEESE ||
      shuffing === STUFFING_POTATO ||
      shuffing === STUFFING_SALAD
    ) {
      this._shuffing = shuffing;
    } else {
      console.log(
        new Error(
          "Об'єкт не створено. Введено неіснуючі дані (введіть STUFFING_CHEESE або STUFFING_POTATO або STUFFING_SALAD)"
        )
      );
    }
    try {
      if (!shuffing) {
        throw new Error(
          "Не введено або введено неіснуючі дані (введіть STUFFING_CHEESE або STUFFING_POTATO або STUFFING_SALAD)"
        );
      }
    } catch (error) {
      console.log(error);
    }
  }
  getSize() {
    return this._size.id;
  }
  getShuffing() {
    return this._shuffing.id;
  }

  //Функція для добавки топпінга
  addTopping(topping) {
    //забороняємо дублювання
    for (let top of Object.values(this.topping)) { 
      if (top.id === topping.id) {
        console.log("Ви не можете це добавити бо воно вже є");
        return;        
      }
    }
    if (topping === TOPPING_MAYO || topping === TOPPING_SPICE)
      this.topping.push(topping);
    else {
      console.log(
        new Error(
          "Топпінг не додано. Неправильні дані (введіть TOPPING_MAYO або TOPPING_SPICE)"
        )
      );
    }
  }
  //Отримати топпінг, реалізуємо по id(містить і назву). Поверне масив з об'єктами
  getTopping() {
    return this.topping;
  }
  //Pеалізуємо функцію видалення топінгів 
  removeTopping(topping) {
    if (topping === TOPPING_MAYO || topping === TOPPING_SPICE) {
      this.topping = this.topping.filter((el) => el.id != topping.id);
    } else console.log(
      new Error(
        "Топпінг не додано. Неправильні дані (введіть TOPPING_MAYO або TOPPING_SPICE)"
      )
    );
  }
  //Функція підрахунку ціни
  calccalculatePrice() {
    let sumMain = 0;
    let sumTopping = 0;
    for (let item of Object.values(this)) {
      if (item.hr) {
        sumMain += item.hr;
        console.log(sumMain);
      }
    }
    for (let item of Object.values(this.topping)) {
      if (item.hr) {
        sumTopping += item.hr;
        console.log(sumTopping);
      }
    }
    return sumMain + sumTopping;
  }

  //Підрахуєто калорії
  calculateCalories() {
    let sumMain = 0;
    let sumTopping = 0;
    for (let item of Object.values(this)) {
      if (item.kal) {
        sumMain += item.kal;
        console.log(sumMain);
      }
    }
    for (let item of Object.values(this.topping)) {
      if (item.kal) {
        sumTopping += item.kal;
        console.log(sumTopping);
      }
    }
    return sumMain + sumTopping;
  }
}

//Робота з DOM

function changeSize () {
  if(document.querySelector("#size").value === "small")
  return SIZE_SMALL;
  if(document.querySelector("#size").value === "large")
  return SIZE_LARGE;
}

function changeShuffing () {
  if(document.querySelector("#shuffing").value === "cheese")
  return STUFFING_CHEESE;
  if(document.querySelector("#shuffing").value === "salad")
  return STUFFING_SALAD;
  if(document.querySelector("#shuffing").value === "potato")
  return STUFFING_POTATO;
}

function createOrder() {
  let hamburger = new Hamburger(changeSize(), changeShuffing());
  if(document.querySelector("#mayo").checked === true) {
    hamburger.addTopping(TOPPING_MAYO);
  }
  if(document.querySelector("#spice").checked === true) {
    hamburger.addTopping(TOPPING_SPICE);
  }
  document.querySelector("#order-price").innerHTML = `Ціна: ${hamburger.calccalculatePrice()} грн.`;
  document.querySelector("#kkal-calc").innerHTML = `Калорії: ${hamburger.calculateCalories()} калорій`
  return hamburger;
}

window.onload = () => createOrder();
document.querySelector("#hamb-order").addEventListener("change", createOrder)

if(!window.localStorage.order) {
  window.localStorage.order = JSON.stringify([]);
}


//Зберігаємо об'єкт замовлення та дату створення в LocalStorage
document.querySelector("#submit").addEventListener("click", (e) => {
  let temp = JSON.parse(window.localStorage.order);
  let now = new Date();
  window.localStorage.order = JSON.stringify([...temp, { ...createOrder(), now }]);
  //e.preventDefault();
});

const submit = document.querySelector("#submit");
submit.addEventListener("mouseover", (e) => {
  let div = document.createElement("div");
  div.innerHTML = "window.localStorage.order";
  div.style.position = "absolute";
  div.style.zIndex = 10;
  moveAt(e.clientX, e.clientY);
  function moveAt(pageX, pageY) {
    div.style.left = pageX + 10 + "px";
    div.style.top = pageY + 10 + "px";
  }
  function onMouseMove(e) {
    moveAt(e.clientX, e.clientY);
  }
  submit.addEventListener("mousemove", onMouseMove);
  document.body.append(div);
  div.classList.add("tooltip");
  console.log(e.clientX + ", " + e.clientY);
  submit.addEventListener("mouseout", (e) => {
    div.remove();
  })
})